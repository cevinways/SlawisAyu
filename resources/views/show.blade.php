@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Orderan Saya</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        <strong>Order ID:</strong>{{ $order->id }} <br>
                        <strong>Ukuran:</strong>{{ $order->ukuran }} <br>
                        <strong>Toping:</strong>{{ $order->toping }} <br>

                        @if ($order->tambahan != '')
                            <strong>Tambahan:</strong>{{$order->tambahan}}
                        @endif
                    </div>

                    <a class="btn btn-primary" href="{{route('pesan.index')}}">Kembali ke Pesanan</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
