@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Orderan Saya</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Alamat</th>
                                    <th>Ukuran</th>
                                    <th>Toping</th>
                                    <th>Tambahan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order)   
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ $order->alamat }}</td>
                                        <td>{{ $order->ukuran }}</td>
                                        <td>{{ $order->toping }}</td>
                                        <td>{{ $order->tambahan }}</td>
                                        <td><a href="{{ route('pesan.show', $order) }}">{{ $order->status->name }}</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
