@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Order Martabak</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{route('pesan.store')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label class="col-sm-2">Alamat</label>
                                    <div class="col-sm-10"><input type="text" name="alamat" placeholder="alamat anda" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2">Ukuran</label>
                                    <div class="col-sm-10">
                                        <div><label class="radio-inline"><input type="radio" checked value="small" id="small" name="ukuran"> Kecil</label></div>
                                        <div><label class="radio-inline"><input type="radio" value="medium" id="medium" name="ukuran"> Sedang</label></div>
                                        <div><label class="radio-inline"><input type="radio" value="large" id="large" name="ukuran"> Besar</label></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2">Toping</label>
                                    <div class="col-sm-10">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="toping[]" value="keju" id="keju"> Keju
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="toping[]" value="mesis" id="keju"> Mesis
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="toping[]" value="kacang" id="keju"> Kacang
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2">Tambahan</label>
                                    <div class="col-sm-10"><input type="text" name="tambahan" placeholder="tambahan untuk pesanan" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-success">Order Sekarang</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
