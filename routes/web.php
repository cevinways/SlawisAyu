<?php
use App\Events\OrderStatusChanged;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/fire', function () {
    event(new OrderStatusChanged);
    return 'Fired';
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/pesan', 'PesanController@index')->name('pesan.index');
    Route::get('/pesan/buat', 'PesanController@create')->name('pesan.create');
    Route::post('/pesan', 'PesanController@store')->name('pesan.store');
    Route::get('/pesan/{pesan}', 'PesanController@show')->name('pesan.show');
});

